#!/usr/bin/python3
# -*- coding: utf-8 -*-
# encoding=utf8

import os
import urllib3
import json
import requests
import numpy as np
import csv
from requests.exceptions import ConnectionError
from config import get_config
import pandas as pd
from datetime import datetime
import time

http = urllib3.PoolManager()

configuration = get_config()

thingsboard_host = configuration['host']
username = configuration['username']
password = configuration['password']

sensors = ['temp_avg','temp_max','temp_min','humid_avg','humid_max','humid_min','dew_point_avg',
              'dew_point_max','dew_point_min','pressure_avg','pressure_max','pressure_min','wind_vel','wind_dir',
              'wind_burst','radiation','precipitation','unavailable_data']

repls = ('temp_avg', 'temp_inst'), ('humid_avg', 'umid_inst'), ('humid_max', 'umid_max'), ('humid_min', 'umid_min'), (
'dew_point_avg', 'pto_orvalho_inst'), ('dew_point_max', 'pto_orvalho_max'), ('dew_point_min', 'pto_orvalho_min'), (
        'radiation', 'radiacao'), ('pressure_avg', 'pressao'), ('pressure_max', 'pressao_max'), (
        'pressure_min', 'pressao_min'), ('wind_vel', 'vento_vel'), ('wind_dir', 'vento_direcao'), (
        'wind_burst', 'vento_rajada'), ('precipitation', 'precipitacao')


def get_token():
    
    url = 'http://' + thingsboard_host + '/api/auth/login'

    values = {'username': username,'password': password,}
    token = {'Content-Type': 'application/json', 'Accept':'application/json',}
    result = {}
    json_message = {}

    try:
        result = requests.post(url, json=values)
        json_message = json.loads(result.content)
        token['X-Authorization'] = 'Bearer ' + json_message['token']
        # print( 'Bearer ' + json_message['token'])
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).'        )
    except KeyError:
        print( 'Error: ' + json_message['message'] + '. Verify the parameters (username, password).')
    
    return token

# curl -X POST --header 'Content-Type: application/json' --header 'Accept: ' --header 'X-Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkaWVnby5kZWZiQG91dGxvb2suY29tIiwic2NvcGVzIjpbIlRFTkFOVF9BRE1JTiJdLCJ1c2VySWQiOiJlZmQ5Y2NkMC00MzE2LTExZTgtYWMzMy1jM2IxODZlMzA4NjMiLCJmaXJzdE5hbWUiOiJEaWVnbyIsImxhc3ROYW1lIjoiQmV6ZXJyYSIsImVuYWJsZWQiOnRydWUsInByaXZhY3lQb2xpY3lBY2NlcHRlZCI6dHJ1ZSwiaXNQdWJsaWMiOmZhbHNlLCJ0ZW5hbnRJZCI6ImVmZDk3ZWIwLTQzMTYtMTFlOC1hYzMzLWMzYjE4NmUzMDg2MyIsImN1c3RvbWVySWQiOiIxMzgxNDAwMC0xZGQyLTExYjItODA4MC04MDgwODA4MDgwODAiLCJpc3MiOiJ0aGluZ3Nib2FyZC5pbyIsImlhdCI6MTU0Njk2NDM2MiwiZXhwIjoxNTQ4NzY0MzYyfQ.KEIHXmUNkNhfXr28V0L6oa-lKeLv-RO4ShDk9Y9cmnHOWQoGqNcY_vyz55wMTt1JQX03sRh3xfLya55aykOIdA' -d '{"result": {}, "longitude": {},"setOrExpired": true}' 'http://demo.thingsboard.io/43969180-1050-11e9-a1f6-a3a281c054e4/attributes'

def insert_device(name, type, token):
    url = 'http://' + thingsboard_host + '/api/device'
    result = {}
    device_inputs = {
        'name': name,
        'type': type,
    }
    try:
        result = requests.post(url, json=device_inputs, headers=token)
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).')
    return result.content

def get_all_devices(token):
    url = 'http://' + thingsboard_host + '/api/tenant/devices?limit=1000000'
    devices = {}
    try:
        result = requests.get(url, headers=token)
        json_message = json.loads(result.content)
        devices = json_message['data']
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).'        )
    except KeyError:
        print( 'Error: ' + json_message['message'] + '. Verify the parameters (username, password).')
    return devices

def get_device_by_name(name, token):

    url = 'http://' + thingsboard_host + '/api/tenant/devices?deviceName=' + name
    json_message = {}
    
    try:
        result = requests.get(url, headers=token)
        json_message = json.loads(result.content)
        print( json_message['name'])
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).')
    except KeyError:
        raise Exception(json_message['message'])
    
    return json_message

def get_credentials(device, token):
    url = 'http://' + thingsboard_host + '/api/device/' + device['id']['id'] + '/credentials'
    device_credentials = {}
    try:
        result = requests.get(url, headers=token)
        json_message = json.loads(result.content)
        device_credentials = json_message['credentialsId']
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).' )
    except KeyError:
        raise Exception(json_message['message'])

    return device_credentials

def insert_device_attributes(device, token, keys={}):
    # url = 'http://' + thingsboard_host + '/api/v1/' + device + '/attributes'
    url = 'http://' + thingsboard_host + '/api/plugins/telemetry/DEVICE/' + device['id']['id'] + '/attributes/SHARED_SCOPE'
    
    try:
        requests.post(url, json=keys, headers=token)
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).' )

def get_device_attributes(device, token, keys={}):
    
    url = 'http://' + thingsboard_host + '/api/plugins/telemetry/DEVICE/' + device['id']['id'] + '/values/attributes?keys=' + ','.join('%s'%s for s in keys)

    result = {}
    attributes = []

    try:
        result = requests.get(url, headers=token)
        attributes = json.loads(result.content)
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).')
    
    return attributes

def get_timestamp(_date):
    from datetime import datetime
    import time

    # print( int(_date[0:4]), int(_date[5:7]), int(_date[8:10]), int(_date[11:13]))

    date = datetime(int(_date[0:4]), int(_date[5:7]), int(_date[8:10]), int(_date[11:13]), 00, 00)
    timestamp = time.mktime(date.timetuple())

    # print( datetime.fromtimestamp(timestamp))

    return int(str(int(timestamp)) + '000')

def get_all_stations_from_root(root=''):
    station_data = []
    print( root)
    for root, dirs, files in os.walk(root):
        for file in files:
            with open(os.path.join(root, file), "r") as auto:
                station_data.append(file[:file.rfind('.')])
    return sorted(station_data)

def delete_data_by_key(device_name, token, keys={}):
    device = get_device_by_name(device_name, token)
    url = 'http://' + thingsboard_host + '/api/plugins/telemetry/DEVICE/'+device['id']['id']+'/timeseries/delete?keys=' + ','.join('%s'%s for s in keys) + '&deleteAllDataForKeys=true&rewriteLatestIfDeleted=false'
    try:
        result = requests.delete(url, headers=token)
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).')

def insert_data_to_tb_from_csv(root='/home/bezerra/Workspace/tww/data2/'):
    token = get_token()
    devices = get_all_devices(token)
    stations_code = get_all_stations_from_root(root)

    for station in stations_code:

        station_data = pd.read_csv(root + '' + station + '.csv')
        device_credentials = ''
        
        while True:
            try:
                device_credentials = get_credentials(get_device_by_name(station, token), token)
                # print( device_credentials)
            except Exception as key:
                
                if (str(key) == 'Token has expired'):
                    token = get_token()
                    continue
                
                elif (str(key) == "Requested item wasn't found!"):

                    insert_device(station_data['station'][0], "STATION", token)
                    
                    attributes = {
                        "latitude": float("{0:.6f}".format(station_data['lat'][0])),
                        "longitude": float("{0:.6f}".format(station_data['lon'][0])),
                        "altitude": station_data['alt'][0],
                        "stationName": station_data['city'][0],
                        "stationState": station_data['state'][0],
                        "stationCode": station_data['station'][0],
                    }
                    
                    insert_device_attributes(get_device_by_name(station, token), token, keys=attributes)

                    continue
            break

        for line in range(len(station_data)):
            values = {}
            for s in sensors:
                
                # O pandas inseriu nan para as strings vazias de unavailable_data.
                if str(station_data['unavailable_data'][line]) == 'nan':
                    station_data['unavailable_data'][line] = ''

                if str(station_data[s][line]) != 'nan':
                    values[reduce(lambda a, kv: a.replace(*kv), repls, s)] = reduce(lambda a, kv: a.replace(*kv), repls, str(station_data[s][line]))
                
            telemetry = {}
            # telemetry['ts'] = get_timestamp(station_data['utc'][line])
            telemetry['ts'] = station_data['utc'][line]
            telemetry['values'] = values

            # try:
            #     url = 'http://' + thingsboard_host + '/api/v1/' + device_credentials + '/telemetry'
            #     result = requests.post(url, json=telemetry, headers=token)
            #     print( telemetry)
            # except ConnectionError:
            #     print( 'Failed to establish a connection. Verify the parameters (url).')
            #     print( "Send data to device " + station + " fail!")


def get_data_from_tb_to_csv():

    get_all_data_from_device(stations = ['A301'])
          
    start_date = '2019-07-01 00:00:00'
    end_date = '2019-07-31 23:59:00'
    
    stationData = []
    # station_data.to_csv('/home/bezerra/Workspace/inmet-time-series/csv_data/' + station + '.csv', index=False)
    for root, dirs, files in os.walk("/home/bezerra/Workspace/inmet-time-series/json_data/"):
        for file in files:
            with open(os.path.join(root, file), "r") as auto:
                stationData.append(file.replace('.json', ''))
    stationData = sorted(stationData)

    for station in stationData:
        
        filename = "/home/bezerra/Workspace/inmet-time-series/json_data/"+station+".json"
        if filename:
            with open(filename, 'r') as f:
                data_from_tb = json.load(f)

        date = pd.date_range(start=start_date, end=end_date, freq='60min')
        
        range_timestamp = [get_timestamp(str(_date))for _date in date]

        keys = ['temp_inst', 'temp_max', 'temp_min', 'umid_inst', 'umid_max', 'umid_min', 'pto_orvalho_inst',
             'pto_orvalho_max', 'pto_orvalho_min', 'pressao', 'pressao_max', 'pressao_min', 'vento_vel', 'vento_direcao',
             'vento_rajada', 'radiacao', 'precipitacao', 'unavailable_data']
        # keys = ['unavailable_data']
        
        data_key = {}
        
        for key in keys:
            
            data_value = {}

            try:
                for v in data_from_tb[key]:
                    try:

                        if key == 'unavailable_data':
                            data_value[v['ts']] = str(v['value']).replace(",", " ")
                        else:
                            data_value[v['ts']] = v['value']
                        
                    except:
                        print( 'fail to get ts and value')
                data_key[key] = data_value

            except:
                print( 'fail to get key', key)

        header = ['timestamp']
        data = []

        for timestamp in range_timestamp:
            row = []
            header = ['timestamp']    
            row.append(timestamp)

            for key, value in data_key.iteritems():
                try:
                    row.append(value[timestamp])
                    header.append(key)
                except:
                    row.append(None)
                    header.append(key)

            data.append(row)
        # print( data)
        try:
            with open('/home/bezerra/Workspace/inmet-time-series/csv_data/'+station + '.csv', 'wb') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
                spamwriter.writerow(header)
                for i in range(len(data)):
                    spamwriter.writerow(data[i])
        finally:
            print( 'Successful')

def get_data_from_tb(device, keys=[], startTs='', endTs='', interval='60000', limit='200000', agg='NONE'):

    token = get_token()
    device = get_device_by_name(device, token)
    _url = 'http://' + thingsboard_host + '/api/plugins/telemetry/DEVICE/' + device['id']['id'] + '/values/timeseries?keys=' + ','.join(keys) + '&startTs=' + startTs + '&endTs=' + endTs + '&interval=' + interval + '&limit=' + limit + '&agg=' + agg + ''
    print( _url)
    result = {}
    try:
        result = requests.get(_url, headers=token)
        return json.loads(result.content)
    except:
        print( requests.get(_url, headers=token))


#- Outono: de 20 de março (13h15) a 21 de junho (7h07). 
#- Inverno: de 21 de junho (7h07) a 22 de setembro (22h54). 
#- Primavera: de 22 de setembro (22h54) a 21 de dezembro (19h23).
#- Verão: de 21 de dezembro (19h23) a 20 de março de 2019 (18h58).       

def get_all_data_from_device(stations = [device['name'] for device in get_all_devices(get_token())]):

    keys = ['temp_inst', 'temp_max', 'temp_min', 'umid_inst', 'umid_max', 'umid_min', 'pto_orvalho_inst',
           'pto_orvalho_max', 'pto_orvalho_min', 'pressao', 'pressao_max', 'pressao_min', 'vento_vel', 'vento_direcao',
           'vento_rajada', 'radiacao', 'precipitacao', 'unavailable_data']
    # keys = ['unavailable_data']
    
    startTs = str(get_timestamp('2019-06-30 23:59:00'))
    endTs = str(get_timestamp('2019-08-01 00:00:00'))
    
    for station in stations:
        tb_data = get_data_from_tb(station, keys=keys, startTs=startTs, endTs=endTs, agg="NONE")
        with open('json_data/'+station+'.json', 'w') as outfile:
            json.dump(tb_data, outfile)
    
def insert_attributes_to_csv(root='/home/bezerra/Workspace/inmet-time-series/json_data/'):
    
    stations_code = get_all_stations_from_root(root)

    token = get_token()

    for station in stations_code:
        attributes = get_device_attributes(get_device_by_name(station, token), token, keys={'stationCode', 'stationName', 'altitude', 'latitude', 'longitude'})
        attr_values = {}
        for atr in attributes:
            attr_values[atr['key']] = atr['value']
            
        station_data = pd.read_csv(root + '' + station + '.csv')
        try:
            station_data.insert(1, 'station', attr_values['stationCode'])
            station_data.insert(2, 'city', attr_values['stationName'].encode('ascii', 'ignore').decode('ascii'))
            station_data.insert(3, 'state', attr_values['stationCode'])
            station_data.insert(5, 'altitude', attr_values['altitude'])
            station_data.insert(6, 'latitude', attr_values['latitude'])
            station_data.insert(5, 'longitude', attr_values['longitude'])

        except UnicodeDecodeError:
            print( 'erro: ' + station)
        
        # station_data.to_csv(root + '' + station + '.csv', index=False)
        station_data.to_csv('/home/bezerra/Workspace/inmet-time-series/csv_data/' + station + '.csv', index=False)

def launchDate_to_timestamp(launchDate):

    date = datetime(int(launchDate[6:]), int(launchDate[3:5]), int(launchDate[0:2]), 00, 00, 00)
    timestamp = time.mktime(date.timetuple())
    
    return str(int(timestamp)) + '000'

if __name__ == '__main__':

    # insert_data_to_tb_from_csv()
    # get_data_from_tb_to_csv()

    stations_missing_attributes = []

    stations = get_all_stations_from_root('data/')

    for station in stations: 
        
        token = get_token()
        attributes = get_device_attributes(get_device_by_name(station, token), token, keys=['launchDate', 'ommCode'])
    
        station_data = pd.read_csv("data/"+station+".csv")

        attr_values = {}

        for atr in attributes:
            attr_values[atr['key']] = [atr['value'] for i in range(len((station_data['temp_avg'])))]

        try:
            
            attr_values.update({'launchDate': [launchDate_to_timestamp(i) for i in attr_values['launchDate']]})
            station_data.insert(1, 'ommCode', attr_values['ommCode'])
            station_data.insert(3, 'launchDate', attr_values['launchDate'])
            station_data.insert(8, 'utc_timestamp', [get_timestamp(i) for i in station_data['utc']])
        
        except KeyError:
            print( 'Failed to get attributes.')
            stations_missing_attributes.append(station)
            pass
    
        station_data.to_csv("data/"+station+".csv", index=False)

    missing_attributes = {'station':stations_missing_attributes}
    missing_attributes.to_csv('stations_missing_attributes')