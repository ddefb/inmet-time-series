#!/usr/bin/python3
# -*- coding: utf-8 -*-

import yaml

def get_config():        
    with open("config.yaml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
        return cfg['tb_demo_access']