#!/usr/bin/python3
# -*- coding: utf-8 -*-
# encoding=utf8

import os
import urllib3
import json
import requests
import numpy as np
import csv
from requests.exceptions import ConnectionError
from config import get_config
import pandas as pd

http = urllib3.PoolManager()

configuration = get_config()

thingsboard_host = configuration['host']
username = configuration['username']
password = configuration['password']

sensors = ['temp_inst','umid_inst','umid_max','umid_min', 'pto_orvalho_inst', 'pto_orvalho_max','pto_orvalho_min','radiacao', 'pressao','pressao_max',
        'pressao_min', 'vento_vel', 'vento_direcao', 'vento_rajada', 'precipitacao', 'unavailable_data']

def get_token():
    
    url = 'http://' + thingsboard_host + '/api/auth/login'

    values = {'username': username,'password': password,}
    token = {'Content-Type': 'application/json', 'Accept':'application/json',}
    result = {}
    json_message = {}

    try:
        result = requests.post(url, json=values)
        json_message = json.loads(result.content)
        token['X-Authorization'] = 'Bearer ' + json_message['token']
        # print( 'Bearer ' + json_message['token'])
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).'        )
    except KeyError:
        print( 'Error: ' + json_message['message'] + '. Verify the parameters (username, password).')
    
    return token

def get_all_devices(token):
    url = 'http://' + thingsboard_host + '/api/tenant/devices?limit=1000000'
    devices = {}
    try:
        result = requests.get(url, headers=token)
        json_message = json.loads(result.content)
        devices = json_message['data']
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).'        )
    except KeyError:
        print( 'Error: ' + json_message['message'] + '. Verify the parameters (username, password).')
    return devices

def get_device_by_name(name, token):

    url = 'http://' + thingsboard_host + '/api/tenant/devices?deviceName=' + name
    json_message = {}
    
    try:
        result = requests.get(url, headers=token)
        json_message = json.loads(result.content)
        print( json_message['name'])
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).')
    except KeyError:
        raise Exception(json_message['message'])
    
    return json_message

def get_credentials(device, token):
    url = 'http://' + thingsboard_host + '/api/device/' + device['id']['id'] + '/credentials'
    device_credentials = {}
    try:
        result = requests.get(url, headers=token)
        json_message = json.loads(result.content)
        device_credentials = json_message['credentialsId']
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).' )
    except KeyError:
        raise Exception(json_message['message'])

    return device_credentials

def get_device_attributes(device, token, keys={}):
    
    url = 'http://' + thingsboard_host + '/api/plugins/telemetry/DEVICE/' + device['id']['id'] + '/values/attributes?keys=' + ','.join('%s'%s for s in keys)

    result = {}
    attributes = []

    try:
        result = requests.get(url, headers=token)
        attributes = json.loads(result.content)
    except ConnectionError:
        print( 'Failed to establish a connection. Verify the parameters (url).')
    
    return attributes

def get_timestamp(_date):
    from datetime import datetime
    import time

    # print( int(_date[0:4]), int(_date[5:7]), int(_date[8:10]), int(_date[11:13]))

    date = datetime(int(_date[0:4]), int(_date[5:7]), int(_date[8:10]), int(_date[11:13]), 00, 00)
    timestamp = time.mktime(date.timetuple())

    # print( datetime.fromtimestamp(timestamp))

    return int(str(int(timestamp)) + '000')

def get_all_stations_from_root(root=''):
    station_data = []
    print( root)
    for root, dirs, files in os.walk(root):
        for file in files:
            with open(os.path.join(root, file), "r") as auto:
                station_data.append(file[:file.rfind('.')])
    return sorted(station_data)

def get_data_from_tb_to_csv():

    # get_all_data_from_device(stations = ['A301'])
          
    start_date = '2018-01-01 00:00:00'
    end_date = '2019-12-31 23:59:00'
    
    stationData = ['A001']
    # for root, dirs, files in os.walk("/home/bezerra/Workspace/inmet-time-series/json_data/"):
    #     for file in files:
    #         with open(os.path.join(root, file), "r") as auto:
    #             stationData.append(file.replace('.json', ''))
    # stationData = sorted(stationData)
    # print(stationData)

    stationData = [device['name'] for device in get_all_devices(get_token())]

    for station in stationData[stationData.index('A834'):]:
        
        # print(station)
        # data_from_tb = []
        # filename = "/home/bezerra/Workspace/inmet-time-series/json_data/"+station+".json"
        # if filename:
        #     with open(filename, 'r') as f:
        #         data_from_tb = json.load(f)

        data_from_tb = get_data_from_device(stations = station)
        
        date = pd.date_range(start=start_date, end=end_date, freq='60min')
        
        range_timestamp = [get_timestamp(str(_date))for _date in date]

        keys = ['temp_inst', 'temp_max', 'temp_min', 'umid_inst', 'umid_max', 'umid_min', 'pto_orvalho_inst',
             'pto_orvalho_max', 'pto_orvalho_min', 'pressao', 'pressao_max', 'pressao_min', 'vento_vel', 'vento_direcao',
             'vento_rajada', 'radiacao', 'precipitacao', 'unavailable_data']
        # keys = ['temp_inst']
        
        data_key = {}
        
        for key in keys:
            
            data_value = {}

            try:
                for v in data_from_tb[key]:
                    try:

                        if key == 'unavailable_data':
                            data_value[v['ts']] = str(v['value']).replace(",", " ")
                        else:
                            data_value[v['ts']] = v['value']
                        
                    except:
                        print( 'fail to get ts and value')
                data_key[key] = data_value

            except:
                print( 'fail to get key', key)

        header = ['timestamp']
        data = []
        for timestamp in range_timestamp:
            row = []
            header = ['timestamp']    
            row.append(timestamp)
            
            for key, value in data_key.iteritems():
                try:
                    row.append(value[timestamp])
                    header.append(key)
                except:
                    row.append(None)
                    header.append(key)

            data.append(row)
        # print( data)
        try:
            with open('/home/bezerra/Workspace/inmet-time-series/csv_data/'+station + '.csv', 'wb') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
                spamwriter.writerow(header)
                for i in range(len(data)):
                    spamwriter.writerow(data[i])
        finally:
            print( 'Successful')

def get_data_from_tb(device, keys=[], startTs='', endTs='', interval='60000', limit='20000000', agg='NONE'):

    token = get_token()
    device = get_device_by_name(device, token)
    _url = 'http://' + thingsboard_host + '/api/plugins/telemetry/DEVICE/' + device['id']['id'] + '/values/timeseries?keys=' + ','.join(keys) + '&startTs=' + startTs + '&endTs=' + endTs + '&interval=' + interval + '&limit=' + limit + '&agg=' + agg + ''
    print( _url)
    result = {}
    try:
        result = requests.get(_url, headers=token)
        return json.loads(result.content)
    except:
        print( requests.get(_url, headers=token))

def get_all_data_from_device(stations = [device['name'] for device in get_all_devices(get_token())]):
# def get_all_data_from_device(stations = ['A001']):

    keys = ['temp_inst', 'temp_max', 'temp_min', 'umid_inst', 'umid_max', 'umid_min', 'pto_orvalho_inst',
           'pto_orvalho_max', 'pto_orvalho_min', 'pressao', 'pressao_max', 'pressao_min', 'vento_vel', 'vento_direcao',
           'vento_rajada', 'radiacao', 'precipitacao', 'unavailable_data']

    # keys = ['temp_inst']

    startTs = str(get_timestamp('2017-12-31 23:59:00'))
    endTs = str(get_timestamp('2020-01-01 00:00:00'))
    
    for station in stations:
        tb_data = get_data_from_tb(station, keys=keys, startTs=startTs, endTs=endTs, agg="NONE")
        with open('json_data/'+station+'.json', 'w') as outfile:
            json.dump(tb_data, outfile)
            

def get_data_from_device(stations = 'A001'):

    keys = ['temp_inst', 'temp_max', 'temp_min', 'umid_inst', 'umid_max', 'umid_min', 'pto_orvalho_inst',
           'pto_orvalho_max', 'pto_orvalho_min', 'pressao', 'pressao_max', 'pressao_min', 'vento_vel', 'vento_direcao',
           'vento_rajada', 'radiacao', 'precipitacao', 'unavailable_data']

    # keys = ['temp_inst']

    startTs = str(get_timestamp('2017-12-31 23:59:00'))
    endTs = str(get_timestamp('2020-01-01 00:00:00'))
    
    
    # for station in stations:
    tb_data = get_data_from_tb(stations, keys=keys, startTs=startTs, endTs=endTs, agg="NONE")
    return tb_data
    
    
if __name__ == '__main__':
    # get_all_data_from_device()
    get_data_from_tb_to_csv()