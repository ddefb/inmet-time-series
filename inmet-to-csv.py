#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import re
import csv
import sys
import math
import numpy as np
import pandas as pd
from pathlib import Path

reload(sys)
sys.setdefaultencoding('utf8')

header = ['station','city', 'state', 'alt','lat', 'lon','utc', 'temp_avg', 'temp_max', 'temp_min', 'humid_avg', 'humid_max', 'humid_min',
                 'dew_point_avg', 'dew_point_max', 'dew_point_min', 'pressure_avg', 'pressure_max', 'pressure_min',
                 'wind_vel', 'wind_dir', 'wind_burst', 'radiation', 'precipitation', 'unavailable_data']

def dms2dd(degrees, minutes, direction):
    dd = float(degrees) + float(minutes)/60.0
    if direction == 'S' or direction == 'W':
        dd *= -1
    return dd

def parse_dms(dms):
    parts = re.split('[^\d\w]+', dms)
    return dms2dd(parts[0], parts[1], parts[2])

def unionTwoTables(sheet1, sheet2):

    matrix = []
    A = np.delete(sheet1, np.s_[217:], 1)
    B = np.delete(np.delete(sheet2, np.s_[183:], 1), np.s_[:1], 1)

    for i in range(len(A)):
        line = list(A[i]) + list(B[i])
        matrix.append(line)

    return matrix

def getStationNameAndLocation2015(filename='/home/bezerra/PycharmProjects/INMET_Data/__PE_A322_GARANHUNS.xls2017.xls'):
    filename = filename[filename.find('__'):filename.rfind('.')]
    code = filename[5:9]
    city = filename[10:].capitalize()
    city = city[:city.rfind('.')]
    state = filename[2:4]

    return code, city, state

def getStationNameAndLocation2000(filename='/home/bezerra/PycharmProjects/INMET_Data/__PE_A322_GARANHUNS.xls2017.xls'):
    filename = filename[filename.find('__'):filename.rfind('.')]
    code = filename[5:9]
    city = filename[10:].replace('_', ' ').capitalize()
    # city = city[:city.rfind('.')]
    state = filename[2:4]
    # print code, city, state
    return code, city, state

def getAltLatAndLon(sheet):
    return sheet[5][1].replace('m', '').replace(',', '.'), parse_dms(sheet[6][1]), parse_dms(sheet[7][1])

def matchCollumnsAux(header, sheet_header):
    for i in range(len(header)):
        if not (header[i] == sheet_header[i]):
            # print header[i], sheet_header[i]
            return False
    return True

def matchCollumns(sheet):

    default2015 = [
        'TEMPERATURA DO AR (°C)', 'UMIDADE RELATIVA DO AR (%)', 'TEMPERATURA DO PONTO DE ORVALHO (°C)',
        'TEMPERATURA MÁXIMA   ', 'TEMPERATURA MÍNIMA   ', 'TEMPERATURA MÁXIMA DO PONTO DE ORVALHO (°C)',
        'TEMPERATURA MÍNIMA DO PONTO DE ORVALHO (°C)', 'UMIDADE RELATIVA MAXIMA DO AR (%)', 'UMIDADE RELATIVA MINIMA DO AR (%)',
        'PRESSÃO ATMOSFERICA (hPa)', 'VENTO VELOCIDADE (m/s)', 'PRECIPITAÇÃO (mm)', 'RADIACAO GLOBAL (KJ/M²)',
        'VENTO, DIREÇÃO (graus)', 'VENTO, RAJADA MAXIMA (m/s)', 'PRESSÃO ATMOSFÉRICA MÁXIMA (hPa)',
        'PRESSÃO ATMOSFÉRICA MÍNIMA (hPa)']

    default20152 = [
        'TEMPERATURA DO AR (°C)', 'UMIDADE RELATIVA DO AR (%)', 'TEMPERATURA DO PONTO DE ORVALHO (°C)',
        'TEMPERATURA MAXIMA (°C)', 'TEMPERATURA MINIMA (°C)', 'TEMPERATURA MÁXIMA DO PONTO DE ORVALHO (°C)',
        'TEMPERATURA MÍNIMA DO PONTO DE ORVALHO (°C)', 'UMIDADE RELATIVA MAXIMA DO AR (%)',
        'UMIDADE RELATIVA MINIMA DO AR (%)',
        'PRESSÃO ATMOSFERICA (hPa)', 'VENTO VELOCIDADE (m/s)', 'PRECIPITAÇÃO (mm)', 'RADIACAO GLOBAL (KJ/M²)',
        'VENTO, DIREÇÃO (graus)', 'VENTO, RAJADA MAXIMA (m/s)', 'PRESSÃO ATMOSFÉRICA MÁXIMA (hPa)',
        'PRESSÃO ATMOSFÉRICA MÍNIMA (hPa)']

    default2000 = [
        'TEMPERATURA DO AR (°C)', 'UMIDADE RELATIVA DO AR (%)', 'TEMPERATURA DO PONTO DE ORVALHO (°C)',
        'TEMPERATURA MAXIMA (°C)', 'TEMPERATURA MINIMA (°C)', 'TEMPERATURA MÁXIMA DO PONTO DE ORVALHO (°C)',
        'TEMPERATURA MÍNIMA DO PONTO DE ORVALHO (°C)', 'UMIDADE RELATIVA MAXIMA DO AR (%)',
        'UMIDADE RELATIVA MINIMA DO AR (%)', 'PRESSÃO ATMOSFERICA (hPa)', 'VENTO VELOCIDADE (m/s)',
        'VENTO, DIREÇÃO (graus)', 'RADIACAO GLOBAL (KJ/M²)', 'PRECIPITAÇÃO (mm)',
        'VENTO, RAJADA MAXIMA (m/s)', 'PRESSÃO ATMOSFÉRICA MÁXIMA (hPa)', 'PRESSÃO ATMOSFÉRICA MÍNIMA (hPa)']

    default20002 = [
        'TEMPERATURA DO AR (°C)', 'TEMPERATURA DO PONTO DE ORVALHO (°C)', 'TEMPERATURA MAXIMA (°C)',
        'TEMPERATURA MINIMA (°C)', 'TEMPERATURA MÁXIMA DO PONTO DE ORVALHO (°C)',
        'TEMPERATURA MÍNIMA DO PONTO DE ORVALHO (°C)',
        'UMIDADE RELATIVA MAXIMA DO AR (%)', 'UMIDADE RELATIVA MINIMA DO AR (%)', 'UMIDADE RELATIVA DO AR (%)',
        'PRECIPITAÇÃO (mm)', 'PRESSÃO ATMOSFERICA (hPa)', 'PRESSÃO ATMOSFÉRICA MÁXIMA (hPa)',
        'PRESSÃO ATMOSFÉRICA MÍNIMA (hPa)', 'RADIACAO GLOBAL (KJ/M²)', 'VENTO, RAJADA MAXIMA (m/s)',
        'VENTO VELOCIDADE (m/s)', 'VENTO, DIREÇÃO (graus)'
    ]

    sheet_header = [
        sheet[0][1]  , sheet[0][25] , sheet[0][49] , sheet[0][73] , sheet[0][97] , sheet[0][121],
        sheet[0][145], sheet[0][169], sheet[0][193], sheet[0][217], sheet[0][241], sheet[0][265],
        sheet[0][299], sheet[0][303], sheet[0][327], sheet[0][351], sheet[0][375]
    ]

    if matchCollumnsAux(default2000, sheet_header):
        return 2000
    elif matchCollumnsAux(default20002, sheet_header):
        return 20002
    elif matchCollumnsAux(default2015, sheet_header) or matchCollumnsAux(default20152, sheet_header):
        return 2015
    return 0

def formatMatrix(sheet, station):

    new_sheet = []
    radiation = []

    if matchCollumns(sheet) == 2015:
        for i in range(2, len(sheet)):
            for j in range(1, 25):
                # temp_avg, temp_max, temp_min, umid_avg, umid_max, umid_min,
                # dew_point_avg, dew_point__max, dew_point__min, pressure_avg, pressure_max, pressure_min
                # wind_vel, wind_dir, wind_burst, radiation, precipitaion, unavailable_data
                # if j > 10 and j < 24:
                new_sheet.append(
                    [
                        station.code, station.city, station.state, station.alt, station.lat, station.lon, sheet[i][0].replace(hour=j - 1),
                        sheet[i][j], sheet[i][j + 72], sheet[i][j + 96], sheet[i][j + 24], sheet[i][j + 168],
                        sheet[i][j + 192],
                        sheet[i][j + 48], sheet[i][j + 120], sheet[i][j + 144], sheet[i][j + 216], sheet[i][j + 350],
                        sheet[i][j + 374],
                        sheet[i][j + 240], sheet[i][j + 326], sheet[i][j + 302], 0, sheet[i][j + 264], '',
                    ]
                )

        radiation = [[float(sheet[i][j]) for j in range(289, 289+14)] for i in range(2, len(sheet))]
        # print radiation

    elif matchCollumns(sheet) == 2000:
        for i in range(2, len(sheet)):
            for j in range(1, 25):
                # temp_avg, temp_max, temp_min, umid_avg, umid_max, umid_min,
                # dew_point_avg, dew_point__max, dew_point__min, pressure_avg, pressure_max, pressure_min
                # wind_vel, wind_dir, wind_burst, radiation, precipitaion, unavailable_data
                # if j > 10 and j < 24:

                new_sheet.append(
                    [
                        station.code, station.city, station.state, station.alt, station.lat, station.lon,
                        sheet[i][0].replace(hour=j - 1),
                        sheet[i][j], sheet[i][j + 72], sheet[i][j + 96], sheet[i][j + 24], sheet[i][j + 168],
                        sheet[i][j + 192],
                        sheet[i][j + 48], sheet[i][j + 120], sheet[i][j + 144], sheet[i][j + 216], sheet[i][j + 350],
                        sheet[i][j + 374],
                        sheet[i][j + 240], sheet[i][j + 264], sheet[i][j + 326], 0, sheet[i][j + 302], '',
                     ]
                )

        radiation = [[float(sheet[i][j]) for j in range(289, 289 + 14)] for i in range(2, len(sheet))]
        # print radiation

    # elif matchCollumns(sheet) == 20002:
    #     for i in range(2, len(sheet)):
    #         for j in range(1, 25):
    #             # temp_avg, temp_max, temp_min, umid_avg, umid_max, umid_min,
    #             # dew_point_avg, dew_point__max, dew_point__min, pressure_avg, pressure_max, pressure_min
    #             # wind_vel, wind_dir, wind_burst, radiation, precipitaion, unavailable_data
    #             # if j > 10 and j < 24:
    #
    #             new_sheet.append(
    #                 [
    #                     station.code, station.city, station.state, station.alt, station.lat, station.lon,
    #                     sheet[i][0].replace(hour=j - 1),
    #                     sheet[i][j], sheet[i][j + 48], sheet[i][j + 72], sheet[i][j + 192], sheet[i][j + 144],
    #                     sheet[i][j + 168],
    #                     sheet[i][j + 24], sheet[i][j + 96], sheet[i][j + 120], sheet[i][j + 240], sheet[i][j + 264],
    #                     sheet[i][j + 288],
    #                     sheet[i][j + 350], sheet[i][j + 374], sheet[i][j + 326], 0, sheet[i][j + 216], '',
    #                 ]
    #             )
    #
    #     radiation = [[float(sheet[i][j]) for j in range(313, 313 + 14)] for i in range(2, len(sheet))]
    #     # print radiation
    else:
        print 'Does not match!', sheet[0]
        raise Exception


    r = []
    for i in range(len(radiation)):
        for k in range(9):
            r.append(float('nan'))
        for j in range(len(radiation[i])):
            r.append(radiation[i][j])
        for k in range(1):
            r.append(float('nan'))

    for i in range(len(new_sheet)):
        new_sheet[i][-3] = r[i]

    for i in range(len(new_sheet)):
        for j in range(len(new_sheet[i])):
            if j > 6 and j < 24:

                if math.isnan(float(new_sheet[i][j])):
                    new_sheet[i][24] = new_sheet[i][24] + header[j] + ' '

    return new_sheet

def getAllDataFromRoot(root='/home/bezerra/PycharmProjects/INMET_Data/2000-2014/'):
    stationData = []
    # print root
    for root, dirs, files in os.walk(root):
        for file in files:
            with open(os.path.join(root, file), "r") as auto:
                stationData.append(root + '/' + file)
    # print sorted(stationData)
    return sorted(stationData)

def main(file1, file2):

    sheet1 = pd.read_excel(file1, header=None).as_matrix()
    sheet2 = pd.read_excel(file2, header=None).as_matrix()

    sheet = unionTwoTables(sheet1, sheet2)

    alt, lat, lon = getAltLatAndLon(sheet)

    code, city, state = getStationNameAndLocation2000(file1) if matchCollumns(sheet[9:]) == 2000 else getStationNameAndLocation2015(file1)

    station = Station(code, city, state, str(alt), lat, lon)

    new_matrix = formatMatrix(sheet[9:], station)

    filename = '/home/bezerra/PycharmProjects/INMET_Data/data/' + station.code # + '-' + city + '-' + state

    if Path(filename + '.csv').is_file():
        with open(filename + '.csv', 'a') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for i in range(len(new_matrix)):
                spamwriter.writerow(new_matrix[i])
    else:
        with open(filename + '.csv', 'a') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(header)
            for i in range(len(new_matrix)):
                spamwriter.writerow(new_matrix[i])

# data_2000_2014 = getAllDataFromRoot('/home/bezerra/PycharmProjects/INMET_Data/2000-2014/')
# data_2015_2016 = getAllDataFromRoot('/home/bezerra/PycharmProjects/INMET_Data/2015-2016/')
# data_2017 = getAllDataFromRoot('/home/bezerra/PycharmProjects/INMET_Data/2017/')

# data1 = ['/home/bezerra/PycharmProjects/INMET_Data/2000-2014/NORDESTE/PE/__PE_A301_RECIFE.xls', '/home/bezerra/PycharmProjects/INMET_Data/2000-2014/NORDESTE/PE/__PE_A301_RECIFE_.xls']
# data2 = ['/home/bezerra/PycharmProjects/INMET_Data/2015-2016/NORDESTE/PE_2016/__PE_A301_RECIFE.xls.xls', '/home/bezerra/PycharmProjects/INMET_Data/2015-2016/NORDESTE/PE_2016/__PE_A301_RECIFE_.xls.xls']
# data3 = ['/home/bezerra/PycharmProjects/INMET_Data/2017/NORDESTE_2017/PE_2017/__PE_A301_RECIFE.xls.xls', '/home/bezerra/PycharmProjects/INMET_Data/2017/NORDESTE_2017/PE_2017/__PE_A301_RECIFE_.xls.xls']

# print data_2000_2014.index("/home/bezerra/PycharmProjects/INMET_Data/2000-2014/SUL/RS/__RS_A804_SANTANA DO LIVRAMENTO.xls")
# print data_2000_2014[848:850]
# data_2000_2014 = data_2000_2014[848:850]

# for i in range(0, len(data_2000_2014), 2):
#     print data_2000_2014[i]
#     print data_2000_2014[i+1]
#     main(data_2000_2014[i], data_2000_2014[i+1])

# for i in range(0, len(data_2015_2016), 2):
#     print data_2015_2016[i]
#     print data_2015_2016[i+1]
#     main(data_2015_2016[i], data_2015_2016[i+1])
#
# for i in range(0, len(data_2017), 2):
#     print data_2017[i]
#     print data_2017[i+1]
#     main(data_2017[i], data_2017[i+1])

default2000 = [
        'TEMPERATURA DO AR (°C)', 'UMIDADE RELATIVA DO AR (%)', 'TEMPERATURA DO PONTO DE ORVALHO (°C)',
        'TEMPERATURA MAXIMA (°C)', 'TEMPERATURA MINIMA (°C)', 'TEMPERATURA MÁXIMA DO PONTO DE ORVALHO (°C)',
        'TEMPERATURA MÍNIMA DO PONTO DE ORVALHO (°C)', 'UMIDADE RELATIVA MAXIMA DO AR (%)',
        'UMIDADE RELATIVA MINIMA DO AR (%)', 'PRESSÃO ATMOSFERICA (hPa)', 'VENTO VELOCIDADE (m/s)',
        'VENTO, DIREÇÃO (graus)', 'RADIACAO GLOBAL (KJ/M²)', 'PRECIPITAÇÃO (mm)',
        'VENTO, RAJADA MAXIMA (m/s)', 'PRESSÃO ATMOSFÉRICA MÁXIMA (hPa)', 'PRESSÃO ATMOSFÉRICA MÍNIMA (hPa)']
